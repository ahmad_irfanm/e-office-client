/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleLetterCodeState.js'
import mutations from './moduleLetterCodeMutations.js'
import actions from './moduleLetterCodeActions.js'
import getters from './moduleLetterCodeGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
