/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './modulePurchaseState.js'
import mutations from './modulePurchaseMutations.js'
import actions from './modulePurchaseActions.js'
import getters from './modulePurchaseGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
