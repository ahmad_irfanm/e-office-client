/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleOutgoingLetterState.js'
import mutations from './moduleOutgoingLetterMutations.js'
import actions from './moduleOutgoingLetterActions.js'
import getters from './moduleOutgoingLetterGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
