/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleInventoryState.js'
import mutations from './moduleInventoryMutations.js'
import actions from './moduleInventoryActions.js'
import getters from './moduleInventoryGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
