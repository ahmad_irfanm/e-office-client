/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleQuotationState.js'
import mutations from './moduleQuotationMutations.js'
import actions from './moduleQuotationActions.js'
import getters from './moduleQuotationGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
