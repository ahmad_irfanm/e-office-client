/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleProformaInvoiceState.js'
import mutations from './moduleProformaInvoiceMutations.js'
import actions from './moduleProformaInvoiceActions.js'
import getters from './moduleProformaInvoiceGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
