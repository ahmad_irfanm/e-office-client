/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleTodoListState.js'
import mutations from './moduleTodoListMutations.js'
import actions from './moduleTodoListActions.js'
import getters from './moduleTodoListGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
