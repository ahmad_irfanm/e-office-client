/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleInvoiceCustomerState.js'
import mutations from './moduleInvoiceCustomerMutations.js'
import actions from './moduleInvoiceCustomerActions.js'
import getters from './moduleInvoiceCustomerGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
