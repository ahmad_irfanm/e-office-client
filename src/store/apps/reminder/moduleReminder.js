/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleReminderState.js'
import mutations from './moduleReminderMutations.js'
import actions from './moduleReminderActions.js'
import getters from './moduleReminderGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
