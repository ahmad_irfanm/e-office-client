/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleViewReportState.js'
import mutations from './moduleViewReportMutations.js'
import actions from './moduleViewReportActions.js'
import getters from './moduleViewReportGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
