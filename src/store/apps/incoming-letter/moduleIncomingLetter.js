/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleIncomingLetterState.js'
import mutations from './moduleIncomingLetterMutations.js'
import actions from './moduleIncomingLetterActions.js'
import getters from './moduleIncomingLetterGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
