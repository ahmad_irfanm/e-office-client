import recipe from "@/recipe";

// const uploadPath = 'https://localhost:8000/upload';
// const uploadPath = 'https://10.10.10.21:8083/upload';
const uploadPath = `${recipe}/upload`;

const storage = {
  incomingLetter: `${uploadPath}/incoming-letters/`,
  outgoingLetter: `${uploadPath}/outgoing-letters/`,
  invoiceCustomer: `${uploadPath}/invoice-customers/`,
  purchase: `${uploadPath}/purchases/`,
  avatar: `${uploadPath}/avatar/`,
};

export default storage;
