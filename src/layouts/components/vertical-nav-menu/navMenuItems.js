/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  // {
  //   url: "/apps/email",
  //   name: "Email",
  //   slug: "email",
  //   icon: "MailIcon",
  //   i18n: "Email",
  // },
  {
    url: '/dashboard',
    name: 'Dashboard',
    slug: 'dashboard',
    tagColor: 'warning',
    icon: 'HomeIcon',
    i18n: 'Dashboard',
  },
  {
    header: 'Primary Menu',
    icon: 'PackageIcon',
    i18n: 'PrimaryMenus',
    items: [
      {
        url: null,
        name: 'Administration',
        icon: 'PackageIcon',
        i18n: 'Administration',
        submenu:  [
          {
            url: '/incoming-letter',
            name: 'IncomingLetter',
            slug: 'incoming-letter',
            i18n: 'IncomingLetter',
          },
          {
            url: '/outgoing-letter',
            name: 'OutGoingLetter',
            slug: 'outgoing-letter',
            i18n: 'OutGoingLetter'
          },
          {
            url: '/invoice-customer',
            name: 'InvoiceCustomer',
            slug: 'invoice-customer',
            icon: 'FileTextIcon',
            i18n: 'InvoiceCustomer'
          },
          {
            url: '/proforma-invoice',
            name: 'ProformaInvoice',
            slug: 'proforma-invoice',
            icon: 'FileIcon',
            i18n: 'ProformaInvoice'
          },
          {
            url: '/purchase-order-customer',
            name: 'PurchaseOrderCustomer',
            slug: 'purchase-order-customer',
            icon: 'ShoppingCartIcon',
            i18n: 'PurchaseOrderCustomer'
          },
          {
            url: '/purchase-to-principle',
            name: 'PurchaseToPrinciple',
            slug: 'purchase-to-principle',
            icon: 'ShoppingCartIcon',
            i18n: 'PurchaseToPrinciple'
          },
          {
            url: '/customer',
            name: 'Customer',
            slug: 'customer',
            icon: 'UsersIcon',
            i18n: 'Customer'
          },
          {
            url: '/request-driver',
            name: 'RequestDriver',
            slug: 'request-driver',
            icon: 'PhoneOutgoingIcon',
            i18n: 'RequestDriver'
          },
          {
            url: '/request-messenger',
            name: 'RequestMessenger',
            slug: 'request-messenger',
            icon: 'CornerUpRightIcon',
            i18n: 'RequestMessenger'
          },
          {
            url: '/reminder',
            name: 'Reminder',
            slug: 'reminder',
            icon: 'CheckCircleIcon',
            i18n: 'Reminder'
          },
        ]
      },
      {
        url: null,
        name: 'Technical',
        icon: 'ToolIcon',
        i18n: 'Technical',
        submenu: [
          {
            url: '/technical/equipment',
            name: 'Equipment',
            slug: 'equipment',
            i18n: 'Equipment',
            submenu: [
              {
                url: '/technical/equipment/status',
                name: 'EquipmentStatus',
                slug: 'equipment-status',
                i18n: 'Status',
              },
              {
                url: '/technical/equipment/brand',
                name: 'EquipmentBrand',
                slug: 'equipment-brand',
                i18n: 'Brand',
              },
              {
                url: '/technical/equipment/equipment',
                name: 'EquipmentEquipment',
                slug: 'equipment-equipment',
                i18n: 'Equipment',
              },
              {
                url: '/technical/equipment/delivered',
                name: 'EquipmentDelivered',
                slug: 'equipment-delivered',
                i18n: 'Delivered',
              },
            ]
          },
          {
            url: '/technical/monthly-report',
            name: 'MonthlyReport',
            slug: 'monthly-report',
            i18n: 'MonthlyReport',
            submenu: [
              {
                url: '/technical/monthly-report/hutchison-3',
                name: 'Hutchison3',
                slug: 'hutchison-3',
                i18n: 'Hutchison3',
              },
              {
                url: '/technical/monthly-report/indosat',
                name: 'Indosat',
                slug: 'indosat',
                i18n: 'Indosat',
              },
            ]
          },
          {
            url: '/technical/techteam-report',
            name: 'TechTeamReport',
            slug: 'techteam-report',
            i18n: 'TechTeamReport',
          },
          {
            url: '/technical/learning-center',
            name: 'LearningCenter',
            slug: 'learning-center',
            i18n: 'LearningCenter',
          },
          {
            url: '/technical/helpdesk-schedule',
            name: 'HelpdeskSchedule',
            slug: 'helpdesk-schedule',
            i18n: 'HelpdeskSchedule',
          },
        ]
      },
      {
        url: null,
        name: 'Sales',
        icon: 'BriefcaseIcon',
        i18n: 'Sales',
        submenu: [
          {
            url: '/sales/dashboard',
            name: 'SalesDashboard',
            slug: 'sales-dashboard',
            i18n: 'Dashboard',
          },
          {
            url: '/sales/forecast',
            name: 'Forecast',
            slug: 'forecast',
            i18n: 'Forecast',
          },
          {
            url: '/sales/customer-contact',
            name: 'CustomerContact',
            slug: 'customer-contact',
            i18n: 'CustomerContact',
          },
          {
            url: null,
            name: 'SalesReport',
            slug: 'sales-report',
            i18n: 'Report',
            submenu: [
              {
                url: '/sales/report/visit',
                name: 'SalesVisitReport',
                slug: 'sales-visit-report',
                i18n: 'VisitReport',
              },
              {
                url: '/sales/report/mom',
                name: 'SalesMomReport',
                slug: 'sales-mom-report',
                i18n: 'MomReport',
              },
              {
                url: '/sales/report/progress',
                name: 'SalesProgressReport',
                slug: 'sales-progress-report',
                i18n: 'ProgressReport',
              },
            ]
          },
          {
            url: null,
            name: 'SalesPO',
            slug: 'sales-purchase-order',
            i18n: 'PurchaseOrder',
            submenu: [
              {
                url: '/sales/purchase-order/from_forecast',
                name: 'SalesPOFromForecast',
                slug: 'sales-purchase-order-from-forecast',
                i18n: 'FromForecast',
              },
              {
                url: '/sales/purchase-order/not_from_forecast',
                name: 'SalesPONotFromForecast',
                slug: 'sales-purchase-order-not-from-forecast',
                i18n: 'NotFromForecast',
              },
            ]
          },
          // {
          //   url: null,
          //   name: 'SalesAdmin',
          //   slug: 'sales-admin',
          //   i18n: 'Admin',
          //   submenu: [
          //     {
          //       url: '/sales/admin/privileges',
          //       name: 'SalesAdminPrivileges',
          //       slug: 'sales-admin-privileges',
          //       i18n: 'Privileges',
          //     },
          //     {
          //       url: '/sales/admin/accounts',
          //       name: 'SalesAdminAccount',
          //       slug: 'sales-admin-account',
          //       i18n: 'Accounts',
          //     },
          //     {
          //       url: '/sales/admin/merk',
          //       name: 'SalesAdminMerk',
          //       slug: 'sales-admin-merk',
          //       i18n: 'Merk',
          //     },
          //     {
          //       url: '/sales/admin/customer',
          //       name: 'SalesAdminCustomer',
          //       slug: 'sales-admin-customer',
          //       i18n: 'Customer',
          //     },
          //     {
          //       url: '/sales/admin/statistic',
          //       name: 'SalesAdminStatistic',
          //       slug: 'sales-admin-statistic',
          //       i18n: 'Statistic',
          //     },
          //   ]
          // },
          {
            url: '/quotation',
            name: 'Quotation',
            slug: 'quotation',
            icon: 'MessageCircleIcon',
            i18n: 'Quotation'
          },
        ]
      }
    ]
  },
  {
    url: '/todo-list',
    name: 'TodoList',
    slug: 'todo-list',
    icon: 'CheckSquareIcon',
    i18n: 'TodoList'
  },
  {
    url: '/view-report',
    name: 'ViewReport',
    slug: 'view-report',
    icon: 'ClipboardIcon',
    i18n: 'ViewReport'
  },
  {
    url: '/job-tracking',
    name: 'JobTracking',
    slug: 'job-tracking',
    icon: 'ActivityIcon',
    i18n: 'JobTracking'
  },
  {
    url: '/employee',
    name: 'Employee',
    slug: 'employee',
    icon: 'UsersIcon',
    i18n: 'EmployeeData'
  },
  {
    header: 'Absent',
    icon: 'PackageIcon',
    i18n: 'Absent',
    items: [
      {
        url: '/absent',
        name: 'SeeAbsent',
        slug: 'see-absent',
        icon: 'CastIcon',
        i18n: 'SeeAbsent'
      },
      {
        url: '/absent/report',
        name: 'ReportAbsent',
        slug: 'report-absent',
        icon: 'PieChartIcon',
        i18n: 'ReportAbsent'
      },
    ]
  }
]

