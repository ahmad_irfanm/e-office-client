// axios
import axios from 'axios'
import i18n from "@/i18n/i18n";
import recipe from "@/recipe";

axios.defaults.baseURL = `${recipe}/api/v1`;
axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('accessToken')}`;
// axios.defaults.headers.common['lang'] = i18n.locale;

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  // Network error
  if (!error.response) {
    error = {
        status: 500,
        data: {
          message: 'Server is not running'
        },
        statusText: 'Internal Server Error'
    };

  } else {
    error = error.response;
  }

  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  return Promise.reject(error);
});

export default axios;
