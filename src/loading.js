
const loading = () => {
  this.$vs.loading({
    type: 'border',
    color: 'primary',
    text: 'Wait a minute :)'
  })
}
